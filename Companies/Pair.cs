﻿using System;
namespace Companies
{
    public delegate void PairEventHandler(int n);

    class Pair<T>
    {
        private T _item1;
        public T Item1
        {
            get
            {
                if (PairOk!= null)
                {
                    PairOk(1);
                }
                return _item1;
            }
            private set
            {
                _item1 = value;
            }
        }

        private T _item2;
        public T Item2
        {
            get
            {
                if (PairOk != null)
                {
                    PairOk(2);
                }
                return _item2;
            }
            private set
            {
                _item2 = value;
            }
        }

        public event PairEventHandler PairOk;

        public Pair(T item1, T item2)
        {
            Item1 = item1;
            Item2 = item2;
        }

    }
}
