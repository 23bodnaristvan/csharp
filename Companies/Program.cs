﻿using System;
using System.Collections;

namespace Companies
{
    class Program
    {
        static void Main(string[] args)
        {

            Company[] companies = new Company[]
            {
                new NonProfit("Champa Masszazs kft.","Ali utca 3."),
                new ForProfit("Iq Trans Europe kft.","Szechenyi ut 6.")
            };

            foreach (Company comp in companies)
            {
                comp.WriteData();
                comp.WriteDescription();
            }

            Stock[] stock = new Stock[]
             {
                new Broker(),
                new ForProfit("ElectroDom kft.","Esztergom ut 8.")
             };

            foreach (Stock item in stock)
            {
                item.BuyStock();
            }  
        
            Pair<string> pair = new Pair<string>("Alap", "Anyag");

            Console.WriteLine(pair.Item1);
            Console.WriteLine(pair.Item2);

        }
    }
}
