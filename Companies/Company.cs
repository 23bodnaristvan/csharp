﻿using System;
namespace Companies
{
    abstract class Company
    {
        public string Name { get; private set; }
        public string Address { get; set; }

        public Company(string name)
        {
            Name = name;
        }

        public Company(string name, string add) : this(name)
        {
            Address = add;
        }

        public void WriteData()
        {
            Console.WriteLine("Company: {0}, Address: {1}", Name, Address);
        }

        public abstract void WriteDescription();

    }
}





