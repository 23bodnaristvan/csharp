﻿using System;
namespace Companies
{
    class NonProfit : Company
    {
        public NonProfit(string name) : base(name)
        {
        }

        public NonProfit(string name, string add) : base(name, add)
        {
        }

        public override void WriteDescription()
        {
            Console.WriteLine("Segítek az embereken.");
        }
    }
}

