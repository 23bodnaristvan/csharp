﻿using System;
namespace Companies
{

    class ForProfit : Company, Stock
    {
        public ForProfit(string name) : base(name)
        {
        }

        public ForProfit(string name, string add) : base(name, add)
        {
        }

        public void BuyStock()
        {
            Console.WriteLine("Részvényt veszek.");
        }

        public override void WriteDescription()
        {
            Console.WriteLine("Pénzt termelek a tulajdonosnak.");
        }
    }
}


